#include <string>
#include <algorithm>
#include <iostream>
#include "find.h"

using namespace std;

void printFind(size_t matchIndex) {
    cout << "--- Find algorithm ---" << endl;
    if(matchIndex != -1) {
        cout << "Match found on index: " << matchIndex << endl;
    }
    else {
        cout << "Match not found." << endl;
    }
}

size_t find(const string& text, const string& pattern) {
    return text.find(pattern);
}