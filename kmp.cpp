#include <iostream>
#include <vector>
#include "kmp.h"

using namespace std;

void printPartialMatchTable(const vector<int>& PMTable) {
    cout << "--- Partial Match Table ---" << endl << "|";
    for (int element: PMTable) {
        cout << element << "|";
    }
    cout << endl;
}

vector<int> getPartialMatchTable(string pattern) {
    vector<int> PMTable;
    int i = 1;
    int j = 0;

    PMTable.push_back(0);

    while (PMTable.size() != pattern.length()) {
        if (pattern[i] == pattern[j]) {
            PMTable.push_back(j + 1);
            i++;
            j++;
        } else {
            if (j != 0) {
                j = PMTable[j - 1];
            } else {
                PMTable.push_back(0);
                i++;
            }
        }
    }
    return PMTable;
}

void printKnuthMorrisPratt(size_t matchIndex) {
    cout << "--- Knuth Morris Pratt ---" << endl;
    if(matchIndex != -1) {
        cout << "Match found on index: " << matchIndex << endl;
    }
    else {
        cout << "Match not found." << endl;
    }
}

size_t getKnuthMorrisPratt(string text, string pattern, vector<int> PMTable) {
    int i = 0;
    int j = 0;
    int matchIndex = -1;

    while(i != text.length()) {
        if(text[i] == pattern[j]) {
            if(matchIndex == -1) {
                matchIndex = i;
            }
            i++;
            j++;
        }
        else {
            if(j != 0) {
                j = PMTable[j - 1];
                if(j != 0) {
                    matchIndex = i - j;
                } else {
                    matchIndex = -1;
                }
            } else {
                matchIndex = -1;
                i++;
            }
        }

        if(matchIndex != -1 && matchIndex + pattern.length() == i) {
            return matchIndex;
        }
    }
    return -1;
}